/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import {
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Images, Colors } from '../Themes'
import styles from './Styles/DrawerScreenStyles'
// import NavCustom from '../Navigation/NavCustom'
export default class DrawerScreen extends React.Component {
  render () {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <View style={styles.viewHead}>
            <View style={styles.viewHeadItem}>
              <View style={styles.viewHeadItemAvatar}>
                <View style={styles.ItemAvatarHead}>
                  <Image source={Images.avatarHome} style={styles.itemAvatar} />
                </View>
                <View style={styles.ItemButtonHead}>
                  <View style={styles.ItemButtonHeadItem}>
                    <TouchableOpacity>

                      <Icon
                        name='more-horiz'
                        size={30}
                        color={Colors.colorPrimary}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={styles.ItemNameHead}>
                <View style={styles.viewFlowHead}>
                  <Text style={styles.textNameHead}>Manh Van Nguyen</Text>
                  <Text style={styles.textUserHead}>@manhnguyenvan</Text>
                </View>
                <View style={styles.viewItemFlow}>
                  <View style={styles.viewItemFlowRowLeft}>
                    <Text style={styles.textFlowHead}>
                    51<Text style={styles.textUserFlowHead}>
                      {' '}
                      Đang theo dõi
                    </Text>
                    </Text>
                  </View>
                  <View style={styles.viewItemFlowRowRight}>
                    <Text style={styles.textFlowHead}>
                    700<Text style={styles.textUserFlowHead}>
                      {' '}
                      Người theo dõi
                    </Text>
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.viewContent}>
            <ScrollView>
              <View style={styles.viewContentItem}>
                <View>
                  <View style={styles.viewItemCT}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Test')} style={styles.viewItemCT}>
                      <View style={styles.viewIconCT}>
                        <Icon
                          name='perm-identity'
                          size={30}
                          color={Colors.darkGray}
                        />
                      </View>
                      <View style={styles.textTextCT}>
                        <Text style={styles.textContent}>Hồ sơ</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.viewItemCT}>
                    <TouchableOpacity style={styles.viewItemCT}>
                      <View style={styles.viewIconCT}>
                        <Icon
                          name='subject'
                          size={30}
                          color={Colors.darkGray}
                    />
                      </View>
                      <View style={styles.textTextCT}>
                        <Text style={styles.textContent}>Danh sách</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.viewItemCT}>
                    <TouchableOpacity style={styles.viewItemCT}>
                      <View style={styles.viewIconCT}>
                        <Icon
                          name='bookmark-border'
                          size={30}
                          color={Colors.darkGray}
                    />
                      </View>
                      <View style={styles.textTextCT}>
                        <Text style={styles.textContent}>Dấu trang</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.viewItemCT}>
                    <TouchableOpacity style={styles.viewItemCT}>
                      <View style={styles.viewIconCT}>
                        <Icon
                          name='gesture'
                          size={30}
                          color={Colors.darkGray}
                    />
                      </View>
                      <View style={styles.textTextCT}>
                        <Text style={styles.textContent}>Khoảnh khắc</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View />
                  <View />
                </View>
                <View style={styles.viewHR} />
                <View >
                  <View style={styles.viewItemCT}>
                    <View style={styles.viewIconCT}>
                      <Text style={styles.textContent}>Cài đặt và riêng tư</Text>
                    </View>
                    <View style={styles.textTextCT} />
                  </View>
                  <View style={styles.viewItemCT}>
                    <View style={styles.viewIconCT}>
                      <Text style={styles.textContent}>Trung tâm trụ giúp</Text>
                    </View>
                    <View style={styles.textTextCT} />
                  </View>
                </View>

              </View>
            </ScrollView>
          </View>
          <View style={styles.viewFooter} >
            <View style={styles.viewLightFT} >
              <Ionicons
                name='ios-moon-outline'
                size={36}
                color={Colors.colorPrimary}
                    />
            </View>
            <View style={styles.viewQRFT} >
              <FontAwesome
                name='qrcode'
                size={36}
                color={Colors.colorPrimary}
                    />
            </View>
          </View>
        </View>
      </View>
    )
  }
}
