import {StyleSheet} from 'react-native'
import { Metrics, Colors, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white
  },
  body: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  itemAvatar: {
    width: Metrics.images.medium1,
    height: Metrics.images.medium1,
    borderRadius: Metrics.images.bmedium1
  },
  viewHeadItemAvatar: {
    paddingRight: 20,
    paddingLeft: 20,
    paddingTop: 30,
    paddingBottom: 10,
    justifyContent: 'center',
    flexDirection: 'row'
  },
  ItemAvatarHead: {
    flex: 0.5
  },
  ItemButtonHead: {
    justifyContent: 'center',
    flex: 0.5,
    alignItems: 'flex-end'
  },
  ItemButtonHeadItem: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 36,
    height: 36,
    borderRadius: 18,
    borderWidth: 2,
    borderColor: Colors.colorPrimary
  },
  textNameHead: {
    fontSize: 20,
    fontFamily: Fonts.type.typeface,
    color: Colors.black,
    fontWeight: '800'
  },
  textUserHead: {
    color: Colors.darkGray,
    fontSize: 16,
    fontFamily: Fonts.type.typeface
  },
  textFlowHead: {
    fontSize: 16,
    fontFamily: Fonts.type.typeface,
    color: Colors.black,
    fontWeight: '700'
  },
  ItemNameHead: {
    paddingRight: 20,
    paddingLeft: 20
  },
  textUserFlowHead: {
    fontFamily: Fonts.type.typeface,
    color: Colors.darkGray,
    fontSize: 16,
    fontWeight: '400'
  },
  viewContent: {
    paddingTop: Metrics.baseMargin
  },
  viewItemFlow: {
    marginTop: Metrics.doubleBaseMargin,
    flexDirection: 'row'
  },
  viewItemFlowRowLeft: {
    flex: 0.5
  },
  viewItemFlowRowRight: {
    alignItems: 'flex-end',
    flex: 0.5
  },
  viewItemCT: {
    alignItems: 'center',
    height: 70,
    // backgroundColor: 'green',
    flexDirection: 'row'
  },
  viewIconCT: {
    paddingLeft: 20
  },
  textContent: {
    fontFamily: Fonts.type.typeface,
    color: Colors.black,
    fontSize: 22,
    fontWeight: '400'
  },
  textTextCT: {
    marginLeft: Metrics.doubleBaseMargin
  },
  viewHR: {
    borderTopColor: Colors.extraLightGray,
    borderTopWidth: 1
  },
  viewFooter: {
    padding: Metrics.doubleBaseMargin,
    flexDirection: 'row'
  },
  viewLightFT: {
    flex: 0.5
  },
  viewQRFT: {
    flex: 0.5,
    alignItems: 'flex-end'
  }

})
