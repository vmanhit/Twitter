import {StyleSheet} from 'react-native'

import {Colors, Fonts, Metrics } from '../../Themes'
export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.white
  },
  body: {
    flex: 1
  },

  itemImage: {
    width: Metrics.images.medium1,
    height: Metrics.images.medium1
  },
  textCancel: {
    fontFamily: Fonts.type.typeface,
    fontSize: 19,
    color: Colors.colorPrimary
  },
  viewHead: {
    padding: 14,
    flex: 0.13,
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  viewCancel: {
    justifyContent: 'center'
  },
  viewMoreIcon: {
    justifyContent: 'center'
  },
  viewHeadLogo: {
    justifyContent: 'center'
  },
  textLogin: {
    fontFamily: Fonts.type.typeface,
    fontWeight: '800',
    color: Colors.black,
    fontSize: 25
  },
  textInput: {
    fontSize: 16,
    color: Colors.darkGray,
    fontFamily: Fonts.type.typeface,
    height: 50,
    borderBottomColor: Colors.extraLightGray,
    borderBottomWidth: 1
  },
  viewInput: {
    flex: 0.9,
    alignItems: 'flex-end'
  },
  viewItemIput: {
    width: '95%'
  },
  viewLogin: {
    flex: 1.5,
    borderTopColor: Colors.extraLightGray,
    borderTopWidth: 1
    // justifyContent: 'center'
  },
  viewLogin1: {
    flex: 0.18,
    borderTopColor: Colors.extraLightGray,
    borderTopWidth: 1
    // justifyContent: 'center'
  },
  viewText: {
    marginBottom: Metrics.doubleBaseMargin
  },
  viewItemLogin: {
    padding: 14,
    flexDirection: 'row'
  },
  viewItemText: {
    justifyContent: 'center',
    flex: 0.6
  },
  viewItemButton: {
    flex: 0.6,
    alignItems: 'flex-end'
  },
  textFGPass: {
    fontFamily: Fonts.type.typeface,
    color: Colors.colorPrimary,
    fontSize: 18
  },
  itemButton: {
    height: 40,
    width: 100,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    backgroundColor: Colors.colorPrimary
  },
  textButton: {
    fontSize: 16,
    color: Colors.white,
    fontFamily: Fonts.type.typeface,
    fontWeight: '600'
  }

})
