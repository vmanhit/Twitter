import {StyleSheet} from 'react-native'

import {Colors, Fonts, Metrics } from '../../Themes'
export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.white
  },
  body: {
    alignContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  viewLogo: {
    width: '80%',
    flex: 0.5

  },
  viewTextBig: {

    alignItems: 'center',
    width: '99%'
  },
  textStart: {
    padding: 25,
    fontSize: 34,
    fontWeight: '800',
    fontFamily: Fonts.type.typeface
  },
  itemImage: {
    marginTop: Metrics.doubleSection,
    width: Metrics.images.medium1,
    height: Metrics.images.medium1
  },
  viewButton: {
    width: '100%'
  },
  textButton: {
    padding: 12,
    color: Colors.white,
    fontSize: 20,
    fontWeight: '600',
    fontFamily: Fonts.type.typeface
  },
  itemButton: {
    alignItems: 'center'
  },
  buttonItem: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.colorPrimary,
    borderRadius: 30,
    width: '77%'
  },
  viewLogin: {
    width: '77%',
    flex: 0.42,
    justifyContent: 'flex-end',
    alignItems: 'flex-start'
  },
  viewLoginText: {
    flexDirection: 'row'
  },
  textLogin: {
    color: Colors.colorPrimary
  },
  textHow: {
    color: Colors.darkGray
  }
})
