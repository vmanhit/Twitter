import {StyleSheet} from 'react-native'
import { Metrics, Colors, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white
  },
  body: {
    flex: 1
  },
  itemAvatar: {
    width: Metrics.images.large,
    height: Metrics.images.large,
    borderRadius: Metrics.images.largeb
  },
  textNameContent: {
    color: Colors.black,
    fontFamily: Fonts.type.typeface,
    fontSize: 17,
    fontWeight: '600'

  },
  textUserNameContent: {
    color: Colors.darkGray,
    fontFamily: Fonts.type.typeface,
    fontSize: 17,
    fontWeight: '300'
  },
  viewHeadContent: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  viewNameContent: {
    flexDirection: 'row'
  },
  viewFrame: {
    padding: 15,
    flexDirection: 'row'
  },
  viewFrameLeft: {
    paddingRight: Metrics.baseMargin,
    flex: 0.2
  },
  viewFrameRight: {
    flex: 0.9
  },
  viewName: {
    paddingRight: Metrics.smallMargin
  },
  UserIconContent: {
    paddingRight: Metrics.smallMargin,
    justifyContent: 'center'
  },
  UserNameContent: {
    paddingRight: Metrics.smallMargin
  },
  textItemConten: {
    color: Colors.black,
    fontFamily: Fonts.type.typeface,
    fontSize: 17,
    fontWeight: '400',
    lineHeight: 19,
    paddingBottom: Metrics.smallMargin
  },
  textLink: {
    color: Colors.colorPrimary,
    fontFamily: Fonts.type.typeface,
    fontSize: 17,
    lineHeight: 19
  },
  viewFormTweet: {
    paddingTop: Metrics.baseMargin,
    width: '88%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  itemIconTweet: {
    height: 17.5,
    width: 18.5
  },
  viewItemTweet: {
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center'
  },
  textTweet: {
    color: Colors.darkGray,
    fontFamily: Fonts.type.typeface,
    fontSize: 15,
    lineHeight: 19
  },
  iTemImageConten: {
    width: Metrics.screenWidth / 1.45,
    height: Metrics.screenHeight / 5,
    borderRadius: 15
  }

})
