import {StyleSheet} from 'react-native'
import { Metrics, Colors, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white
  },
  body: {
    flex: 1
  },
  itemAvatar: {
    width: Metrics.images.medium,
    height: Metrics.images.medium,
    borderRadius: Metrics.images.small
  },
  itemAvatarTweet: {
    marginTop: -Metrics.smallMargin,
    width: Metrics.images.medium,
    height: Metrics.images.medium,
    borderRadius: Metrics.images.small
  },
  textNameContent: {
    color: Colors.black,
    fontFamily: Fonts.type.typeface,
    fontSize: 17,
    fontWeight: '600'

  },
  textUserNameContent: {
    color: Colors.darkGray,
    fontFamily: Fonts.type.typeface,
    fontSize: 17,
    fontWeight: '300'
  },
  viewHeadContent: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  viewNameContent: {
    flexDirection: 'row'
  },
  viewFrame: {
    borderTopColor: Colors.lightGray,
    borderTopWidth: 1,
    padding: 15,
    flexDirection: 'row'
  },
  viewFrameLeft: {
    justifyContent: 'flex-start',
    paddingRight: Metrics.baseMargin,
    flex: 0.2,
    alignItems: 'flex-end'
  },
  viewFrameRight: {
    flex: 0.9
  },
  viewName: {
    paddingRight: Metrics.smallMargin
  },
  UserIconContent: {
    paddingRight: Metrics.smallMargin,
    justifyContent: 'center'
  },
  UserNameContent: {
    paddingRight: Metrics.smallMargin
  },
  textItemConten: {
    color: Colors.darkGray,
    fontFamily: Fonts.type.typeface,
    fontSize: 17,
    fontWeight: '400',
    lineHeight: 19,
    paddingBottom: Metrics.smallMargin
  },
  textLink: {
    color: Colors.colorPrimary,
    fontFamily: Fonts.type.typeface,
    fontSize: 17,
    lineHeight: 19
  },
  viewFormTweet: {
    paddingTop: Metrics.baseMargin,
    width: '88%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  itemIconTweet: {
    height: 17.5,
    width: 18.5
  },
  viewItemTweet: {
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center'
  },
  textTweet: {
    color: Colors.darkGray,
    fontFamily: Fonts.type.typeface,
    fontSize: 15,
    lineHeight: 19
  },
  iTemImageConten: {
    width: Metrics.screenWidth / 1.45,
    height: Metrics.screenHeight / 5,
    borderRadius: 15
  },
  textItemContenUser: {
    fontWeight: '600',
    color: Colors.black,
    fontFamily: Fonts.type.typeface,
    fontSize: 17,
    lineHeight: 19
  },
  textItemContenTweet: {
    color: Colors.black,
    fontFamily: Fonts.type.typeface,
    fontSize: 17,
    lineHeight: 19
  },
  viewTweet: {
    paddingTop: Metrics.smallMargin,
    flexDirection: 'row'
  },
  viewTab: {
    padding: 10,
    alignItems: 'center'

  },
  viewTabItem: {
    width: '90%',
    // borderWidth: 1,
    // borderRadius: 30,
    // borderColor: Colors.colorPrimary,
    flexDirection: 'row',
    flex: 1

  },
  viewTabItemAll: {
    borderRightColor: Colors.colorPrimary,
    borderColor: Colors.colorPrimary,
    alignItems: 'center',
    justifyContent: 'center',
    height: 38,
    flex: 0.5,
    borderTopLeftRadius: 30,
    borderBottomLeftRadius: 30,
    backgroundColor: Colors.colorPrimary
  },
  viewTabItemText: {
    color: Colors.white,
    fontSize: 18,
    fontWeight: '600',
    fontFamily: Fonts.type.typeface
  },
  viewTabItemTextOffer: {

    color: Colors.colorPrimary,
    fontSize: 18,
    fontWeight: '600',
    fontFamily: Fonts.type.typeface
  },
  viewTabItemOffer: {
    marginLeft: -1,
    borderTopRightRadius: 30,
    borderBottomRightRadius: 30,
    borderBottomColor: Colors.colorPrimary,
    borderRightColor: Colors.colorPrimary,
    borderColor: Colors.colorPrimary,
    borderWidth: 1,
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    height: 38
  }

})
