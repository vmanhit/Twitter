import {StyleSheet} from 'react-native'

import {Colors, Fonts, Metrics } from '../../Themes'
export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.white
  },
  body: {
    flex: 1
  },

  textInfo: {
    fontSize: 22,
    color: Colors.black,
    fontFamily: Fonts.type.typeface,
    fontWeight: '800'
  },
  viewHead: {
    padding: 14,
    flex: 0.08,
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderBottomColor: Colors.extraLightGray,
    borderBottomWidth: 1
  },
  viewCancel: {
    justifyContent: 'center'
  },
  viewMoreIcon: {
    justifyContent: 'center'
  },
  viewHeadLogo: {
    justifyContent: 'center'
  },
  viewForm: {
    flex: 1
  },
  buttonTweet: {
    opacity: 0.5,
    borderRadius: 30,
    backgroundColor: Colors.colorPrimary
  },
  buttonTweetOn: {
    borderRadius: 30,
    backgroundColor: Colors.colorPrimary
  },
  textTweet: {
    paddingTop: 7,
    paddingBottom: 7,
    paddingLeft: 10,
    paddingRight: 10,
    color: Colors.white,
    fontWeight: '700',
    fontFamily: Fonts.type.typeface
  },
  viewTextForm: {
    position: 'absolute',
    zIndex: 1,
    padding: 12,
    flexDirection: 'row'
  },
  itemAvatar: {
    width: Metrics.images.medium,
    height: Metrics.images.medium,
    borderRadius: Metrics.images.small,
    marginRight: 15
  },
  viewTextContent: {
    width: '90%'
  },
  textInput: {
    width: '90%',
    fontSize: 16,
    color: Colors.black,
    fontFamily: Fonts.type.typeface,
    height: 40
  },
  viewItemLogin: {
    padding: 14,
    flexDirection: 'row'
  },
  viewItemText: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    flex: 0.8
  },
  viewItemButton: {
    flex: 0.2,
    alignItems: 'flex-end'
  },

  textButton: {
    fontSize: 16,
    color: Colors.white,
    fontFamily: Fonts.type.typeface,
    fontWeight: '600'
  },
  viewLogin: {
    flex: 0.8,
    justifyContent: 'flex-start'
    // marginTop: 20
  },
  viewLogin1: {
    flex: 0.0

  }

})
