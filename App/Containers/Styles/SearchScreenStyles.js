import {StyleSheet} from 'react-native'
import { Metrics, Colors, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white
  },
  body: {
    flex: 1
  },
  itemAvatar: {
    width: Metrics.images.large,
    height: Metrics.images.large,
    borderRadius: Metrics.images.largeb
  },
  textNameHead: {
    color: Colors.black,
    fontFamily: Fonts.type.typeface,
    fontSize: 17,
    fontWeight: '600',
    padding: 14
  },
  textNameTrend: {
    color: Colors.black,
    fontFamily: Fonts.type.typeface,
    fontSize: 16,
    fontWeight: '400',
    paddingBottom: 5
  },
  textNumberTrend: {
    color: Colors.darkGray,
    fontFamily: Fonts.type.typeface,
    fontSize: 16,
    fontWeight: '400'
  },
  textTweetTrend: {
    color: Colors.darkGray,
    fontFamily: Fonts.type.typeface,
    fontSize: 14,
    paddingBottom: 0
  },
  ItemTrend: {
    padding: 14,
    flexDirection: 'row'
  },
  formNumber: {
    alignItems: 'center',
    paddingRight: Metrics.baseMargin
  },
  ItemFormTrend: {
    borderTopColor: Colors.extraLightGray,
    borderTopWidth: 1
  },
  viewFormContent: {
    borderColor: Colors.lightGray,
    borderWidth: 1,
    borderRadius: 8
  },
  itemFormContent: {
    flexDirection: 'row'
  },
  itemImages: {
    width: 80,
    height: 80

  },
  itemTextContent: {

  },
  itemImageContent: {

  }

})
