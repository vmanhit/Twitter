import {StyleSheet} from 'react-native'

import {Colors, Fonts, Metrics } from '../../Themes'
export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.white
  },
  body: {
    flex: 1
  },

  textInfo: {
    fontSize: 22,
    color: Colors.black,
    fontFamily: Fonts.type.typeface,
    fontWeight: '800'
  },
  viewHead: {
    padding: 14,
    flex: 0.08,
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderBottomColor: Colors.extraLightGray,
    borderBottomWidth: 1
  },
  viewCancel: {
    justifyContent: 'center'
  },
  viewMoreIcon: {
    justifyContent: 'center'
  },
  viewHeadLogo: {
    justifyContent: 'center'
  },
  viewForm: {
    flex: 1,
    backgroundColor: Colors.backgroundBL
  },
  viewVer: {
    flexDirection: 'row',
    backgroundColor: Colors.white,
    borderBottomColor: Colors.extraLightGray,
    borderBottomWidth: 1
  },
  viewHelItem: {
    flexDirection: 'row',
    backgroundColor: Colors.white
    // borderBottomColor: Colors.extraLightGray,
    // borderBottomWidth: 1
  },
  viewHel: {
    flexDirection: 'row',
    backgroundColor: Colors.white
  },
  viewHelpCentter: {
    width: '100%',
    flexDirection: 'row',
    backgroundColor: Colors.white
  },
  viewCentter: {
    alignItems: 'flex-end',
    backgroundColor: Colors.white
  },
  textLeft: {
    color: Colors.black,
    fontWeight: '700',
    fontSize: 17,
    padding: 15
  },
  textRight: {
    color: Colors.darkGray,
    fontWeight: '500',
    fontSize: 17,
    padding: 15
  },
  textHelp: {
    color: Colors.darkGray,
    fontWeight: '800',
    fontSize: 24,
    padding: 15
  },
  viewItemTextright: {
    flex: 0.5,
    alignItems: 'flex-end'
  },
  viewItemTextleft: {
    flex: 0.5

  },
  Switch: {
    padding: 14
  },
  textAuto: {
    fontSize: 15,
    paddingRight: 14,
    paddingLeft: 14,
    paddingBottom: 14,
    color: Colors.darkGray
  },
  hr: {
    alignContent: 'flex-end',
    borderBottomColor: Colors.extraLightGray,
    borderBottomWidth: 1,
    width: '96%'
  }

})
