/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView
} from "react-native";
import { ActionSheetCustom as ActionSheet } from "react-native-actionsheet";
import Hyperlink from "react-native-hyperlink";
import Autolink from "react-native-autolink";
import NavCustom from "../Navigation/NavCustom";
import { Images, Colors } from "../Themes";
import styles from "./Styles/SearchScreenStyles";
import Icon from "react-native-vector-icons/MaterialIcons";
const options = [
  "Huỷ",
  "Thêm vào Tweet vào Khoảng khắc",
  "Tôi không tích Tweet này",
  "Ngừng theo dõi @",
  "Tắt tiếng @",
  "Chặn @",
  "Báo cáo Tweet"
];
export default class SearchScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: (
      <NavCustom
        onPress={() => navigation.navigate("DrawerToggle")}
        iconName="person-add"
        tittleHead="Tim kiem"
        checkSearch={true}
        onPressRight={() => navigation.navigate("MyModal")}
      />
    )
  });

  showActionSheet = () => {
    this.ActionSheet.show();
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <View style={styles.viewcContent}>
            <ScrollView>
              <View style={styles.viewItem}>
                <View style={styles.viewHeadName}>
                  <Text style={styles.textNameHead}>Xu hướng cho bạn</Text>
                </View>
                <View style={styles.FormTrend}>
                  <View style={styles.ItemFormTrend}>
                    <View style={styles.ItemTrend}>
                      <View style={styles.formNumber}>
                        <Text style={styles.textNumberTrend}>1</Text>
                      </View>
                      <View>
                        <View>
                          <Text style={styles.textNameTrend}>#Vietnam</Text>
                          <Text style={styles.textTweetTrend}>2.344 Tweet</Text>
                        </View>
                        <View />
                      </View>
                    </View>
                  </View>
                  <View style={styles.ItemFormTrend}>
                    <View style={styles.ItemTrend}>
                      <View style={styles.formNumber}>
                        <Text style={styles.textNumberTrend}>2</Text>
                      </View>
                      <View>
                        <View>
                          <Text style={styles.textNameTrend}>#Vietnam</Text>
                          <Text style={styles.textTweetTrend}>2.344 Tweet</Text>
                        </View>
                        {/* <View style={styles.viewFormContent}>
                          <View style={styles.itemFormContent}>
                            <View style={styles.itemTextContent}>
                              <Text>gogole.com</Text>
                              <Text>Did Debastian</Text>
                            </View>
                            <View style={styles.itemImageContent}>
                              <Image style={styles.itemImages} source={Images.avatarHome} />
                            </View>
                          </View>
                        </View> */}
                      </View>
                    </View>
                  </View>
                  <View style={styles.ItemFormTrend}>
                    <View style={styles.ItemTrend}>
                      <View style={styles.formNumber}>
                        <Text style={styles.textNumberTrend}>3</Text>
                      </View>
                      <View>
                        <View>
                          <Text style={styles.textNameTrend}>#Vietnam</Text>
                          <Text style={styles.textTweetTrend}>2.344 Tweet</Text>
                        </View>
                        <View />
                      </View>
                    </View>
                  </View>
                  <View style={styles.ItemFormTrend}>
                    <View style={styles.ItemTrend}>
                      <View style={styles.formNumber}>
                        <Text style={styles.textNumberTrend}>4</Text>
                      </View>
                      <View>
                        <View>
                          <Text style={styles.textNameTrend}>#Vietnam</Text>
                          <Text style={styles.textTweetTrend}>2.344 Tweet</Text>
                        </View>
                        <View />
                      </View>
                    </View>
                  </View>
                  <View style={styles.ItemFormTrend}>
                    <View style={styles.ItemTrend}>
                      <View style={styles.formNumber}>
                        <Text style={styles.textNumberTrend}>5</Text>
                      </View>
                      <View>
                        <View>
                          <Text style={styles.textNameTrend}>#Vietnam</Text>
                          <Text style={styles.textTweetTrend}>2.344 Tweet</Text>
                        </View>
                        <View />
                      </View>
                    </View>
                  </View>
                  <View style={styles.ItemFormTrend}>
                    <View style={styles.ItemTrend}>
                      <View style={styles.formNumber}>
                        <Text style={styles.textNumberTrend}>5</Text>
                      </View>
                      <View>
                        <View>
                          <Text style={styles.textNameTrend}>#Vietnam</Text>
                          <Text style={styles.textTweetTrend}>2.344 Tweet</Text>
                        </View>
                        <View />
                      </View>
                    </View>
                  </View>
                  <View style={styles.ItemFormTrend}>
                    <View style={styles.ItemTrend}>
                      <View style={styles.formNumber}>
                        <Text style={styles.textNumberTrend}>7</Text>
                      </View>
                      <View>
                        <View>
                          <Text style={styles.textNameTrend}>#Vietnam</Text>
                          <Text style={styles.textTweetTrend}>2.344 Tweet</Text>
                        </View>
                        <View />
                      </View>
                    </View>
                  </View>
                  <View style={styles.ItemFormTrend}>
                    <View style={styles.ItemTrend}>
                      <View style={styles.formNumber}>
                        <Text style={styles.textNumberTrend}>8</Text>
                      </View>
                      <View>
                        <View>
                          <Text style={styles.textNameTrend}>#Vietnam</Text>
                          <Text style={styles.textTweetTrend}>2.344 Tweet</Text>
                        </View>
                        <View />
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}
