import React, { Component } from "react";
import ReactNative, { View,Text,TextInput,Image,TouchableOpacity,Alert,Keyboard,ScrollView} from "react-native";
import styles from "./Styles/LoginScreenStyles";
import { Images, Colors } from "../Themes";
import I18n from "react-native-i18n";
import { validateUid, validateName, required } from "../Utilities/Regex";
import Icon from "react-native-vector-icons/MaterialIcons";
export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: "",
      password: "",
      styleKeyBoard: false
    };
  }
  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = () => {
    this.setState({ styleKeyBoard: true });
  };

  _keyboardDidHide = () => {
    this.setState({ styleKeyBoard: false });
  };
  _validateLogin() {
    const { user, password } = this.state;
    if (!validateUid(user)) {
      Alert.alert("Tên người dùng không được để trống hoặc đúng");
      return false;
    }
    if (!password || password.length < 6) {
      Alert.alert("Mật khẩu không được để trống");
      return false;
    }
    return true;
  }
  _onLogin() {
    if (this._validateLogin()) {
      Keyboard.dismiss();
      const { user, password } = this.state;
      this._onLoginTweeter()
    }
  }
  _onLoginTweeter(){
    Alert.alert("Login");
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.body}>
          <View style={styles.viewHead}>
            <View style={styles.viewCancel}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("StartScreen")}
              >
                <Text style={styles.textCancel}>Huỷ</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.viewHeadLogo}>
              <Image source={Images.logoLogin} style={styles.itemImage} />
            </View>
            <View style={styles.viewMoreIcon}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("MyModal")}
              >
                <Icon name="more-horiz" size={36} color={Colors.colorPrimary} />
              </TouchableOpacity>
            </View>
          </View>
          <ScrollView>
            <View style={styles.viewInput}>
              <View style={styles.viewItemIput}>
                <View style={styles.viewText}>
                  <Text style={styles.textLogin}>Đăng nhập vào Twitter</Text>
                </View>
                <View>
                  <TextInput
                    ref="userLogin"
                    autoCorrect={false}
                    onSubmitEditing={Keyboard.dismiss}
                    placeholder="Điện thoại, email, hoặc tên người dùng"
                    underlineColorAndroid="transparent"
                    style={styles.textInput}
                    onChangeText={user => this.setState({ user })}
                    value={this.state.user}
                    returnKeyType="next"
                    autoCapitalize="none"
                  />
                  <TextInput
                    ref="passLogin"
                    autoCorrect={false}
                    onSubmitEditing={Keyboard.dismiss}
                    placeholder="Mật khẩu"
                    secureTextEntry
                    style={styles.textInput}
                    onChangeText={password => this.setState({ password })}
                    value={this.state.password}
                    returnKeyType="done"
                    autoCapitalize="none"
                   
                  />
                </View>
              </View>
            </View>
          </ScrollView>
          <View
            style={
              this.state.styleKeyBoard ? styles.viewLogin : styles.viewLogin1
            }
          >
            <View style={styles.viewItemLogin}>
              <View style={styles.viewItemText}>
                <TouchableOpacity>
                  <Text style={styles.textFGPass}>Quên mật khẩu?</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.viewItemButton}>
                <TouchableOpacity
                  onPress={() => this._onLogin()}
                  style={styles.itemButton}
                >
                  <Text style={styles.textButton}>Đăng nhập</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
  _scrollLoginToInput (reactNode) {
    this._scrollViewLogin.scrollToFocusedInput(reactNode)
  }
}
