/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView
} from "react-native";
import Hyperlink from "react-native-hyperlink";
import Autolink from "react-native-autolink";
import NavCustom from "../Navigation/NavCustom";
import { Images, Colors } from "../Themes";
import styles from "./Styles/NotificationsScreenStyles";
import Icon from "react-native-vector-icons/MaterialIcons";
export default class NotificationsScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: <NavCustom onPress={() => navigation.navigate("DrawerToggle")} iconName ='settings' tittleHead = 'Thông báo' />
  });
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <View style={styles.viewcContent}>
            <ScrollView>
              <View style={styles.viewItem}>
              <View style={styles.viewTab}>
                <View  style={styles.viewTabItem}>
                    <View style={styles.viewTabItemAll}>
                        <TouchableOpacity style={styles.viewTabItemAll}>
                            <Text style={styles.viewTabItemText}>Tất cả</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.viewTabItemOffer}>
                    <TouchableOpacity style={styles.viewTabItemOffer1}>
                            <Text style={styles.viewTabItemTextOffer}>Đề cập</Text>
                        </TouchableOpacity>
                    </View>
                </View>
              </View>
                <View style={styles.viewFrame}>
                  <View style={styles.viewFrameLeft}>
                    <Image
                      style={styles.itemAvatarTweet}
                      source={Images.logoLogin}
                    />
                  </View>
                  <View style={styles.viewFrameRight}>
                    <View style={styles.viewFrameContentLeft}>
                      <View style={styles.viewHeadContent}>
                        <View style={styles.viewNameContent}>
                          <Image
                            style={styles.itemAvatar}
                            source={Images.avatarHome}
                          />
                        </View>
                        <View style={styles.viewIconSeclect}>
                          <TouchableOpacity>
                            <Icon
                              name="keyboard-arrow-down"
                              size={25}
                              color={Colors.lightGray}
                            />
                          </TouchableOpacity>
                        </View>
                      </View>
                      <View style={styles.viewFormContent}>
                        <View style={styles.viewTweet}>
                          <View>
                            {" "}
                            <Text style={styles.textItemContenTweet}>
                              Trong trường hợp bạn bỏ lỡ Tweet của{" "}
                              <Text style={styles.textItemContenUser}>
                                YANTV
                              </Text>
                            </Text>
                          </View>
                        </View>

                        <Text
                          numberOfLines={3}
                          style={styles.textItemConten} 
                        >
                        Anh khóc vì giờ đây a mất e rồi, a khóc vì giờ đây e đã xa thật rồi, a nhớ lời hẹn ước không xa rời, để giờ đây chúng ta lìa đôi
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
                <View style={styles.viewFrame}>
                  <View style={styles.viewFrameLeft}>
                    <Image
                      style={styles.itemAvatarTweet}
                      source={Images.logoLogin}
                    />
                  </View>
                  <View style={styles.viewFrameRight}>
                    <View style={styles.viewFrameContentLeft}>
                      <View style={styles.viewHeadContent}>
                        <View style={styles.viewNameContent}>
                          <Image
                            style={styles.itemAvatar}
                            source={Images.avatarHome}
                          />
                        </View>
                        <View style={styles.viewIconSeclect}>
                          <TouchableOpacity>
                            <Icon
                              name="keyboard-arrow-down"
                              size={25}
                              color={Colors.lightGray}
                            />
                          </TouchableOpacity>
                        </View>
                      </View>
                      <View style={styles.viewFormContent}>
                        <View style={styles.viewTweet}>
                          <View>
                            {" "}
                            <Text style={styles.textItemContenTweet}>
                              Trong trường hợp bạn bỏ lỡ Tweet của{" "}
                              <Text style={styles.textItemContenUser}>
                                YANTV
                              </Text>
                            </Text>
                          </View>
                        </View>

                        <Text
                          numberOfLines={3}
                          style={styles.textItemConten} 
                        >
                        Anh khóc vì giờ đây a mất e rồi, a khóc vì giờ đây e đã xa thật rồi, a nhớ lời hẹn ước không xa rời, để giờ đây chúng ta lìa đôi
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}
