import React, { Component } from 'react'
import { View, Text, TextInput, Image, ScrollView, Switch, TouchableOpacity, Keyboard } from 'react-native'
import styles from './Styles/InfoTwitterScreenStyles'
import {Images, Colors} from '../Themes'
import Icon from 'react-native-vector-icons/MaterialIcons'
export default class InfoTwitterScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {

      switch1Value: false
    }
  }
  render () {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.body} >
          <View style={styles.viewHead} >
            <View style={styles.viewCancel} />
            <View style={styles.viewHeadLogo}>
              <Text style={styles.textInfo}>Giới thiệu về Tiwtter</Text>
            </View>
            <View style={styles.viewMoreIcon}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Icon name='clear' size={30} color={Colors.colorPrimary} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.viewForm}>
            <ScrollView>
              <View style={styles.viewFormItem}>
                <View style={styles.viewVer} >
                  <View style={styles.viewItemTextleft}>
                    <Text style={styles.textLeft}>Phiên bản</Text>
                  </View>
                  <View style={styles.viewItemTextright}>
                    <Text style={styles.textRight}>7.21.2</Text>
                  </View>
                </View>
              </View>
              <View style={styles.viewHelp}>
                <View style={styles.viewItemHelp}>
                  <Text style={styles.textHelp}>Nhận trợ giúp</Text>
                </View>
                <View style={styles.viewHel} >
                  <View style={styles.viewItemTextleft}>
                    <Text style={styles.textLeft}>Gửi báo cáo sự cố</Text>
                  </View>
                  <View style={styles.viewItemTextright}>
                    <View style={styles.Switch}>
                      <Switch
                        value={this.state.switch1Value} />
                    </View>
                  </View>
                </View>
                <View style={styles.viewHelItem}>
                  <Text style={styles.textAuto}>Tự động gửi báo cáo sự cố đến nhà cung cấp dịch vụ của Twitter để giúp cải thiện ứng dụng này.</Text>
                </View>
                <View style={styles.viewCentter}>
                  <View style={styles.hr} />
                  <View style={styles.viewHelpCentter}>
                    <View style={styles.viewItemTextleft}>
                      <Text style={styles.textLeft}>Trung tâm trợ giúp</Text>
                    </View>
                    <View style={styles.viewItemTextright}>
                      <View style={styles.Switch}>
                        <Icon name='chevron-right' color={Colors.lightGray} size={30} />
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.viewHelp}>
                <View style={styles.viewItemHelp}>
                  <Text style={styles.textHelp}>Pháp lý</Text>
                </View>
                <View style={styles.viewCentter}>
                  <View style={styles.viewHelpCentter}>
                    <View style={styles.viewItemTextleft}>
                      <Text style={styles.textLeft}>Điều khoản dịch vụ</Text>
                    </View>
                    <View style={styles.viewItemTextright}>
                      <View style={styles.Switch}>
                        <Icon name='chevron-right' color={Colors.lightGray} size={30} />
                      </View>
                    </View>
                  </View>
                  <View style={styles.hr} />
                </View>
                <View style={styles.viewCentter}>
                  <View style={styles.viewHelpCentter}>
                    <View style={styles.viewItemTextleft}>
                      <Text style={styles.textLeft}>Chính sách riêng tư</Text>
                    </View>
                    <View style={styles.viewItemTextright}>
                      <View style={styles.Switch}>
                        <Icon name='chevron-right' color={Colors.lightGray} size={30} />
                      </View>
                    </View>
                  </View>
                  <View style={styles.hr} />
                </View>
                <View style={styles.viewCentter}>
                  <View style={styles.viewHelpCentter}>
                    <View style={styles.viewItemTextleft}>
                      <Text style={styles.textLeft}>Sửu dụng cookie</Text>
                    </View>
                    <View style={styles.viewItemTextright}>
                      <View style={styles.Switch}>
                        <Icon name='chevron-right' color={Colors.lightGray} size={30} />
                      </View>
                    </View>
                  </View>
                  <View style={styles.hr} />
                </View>
                <View style={styles.viewCentter}>
                  <View style={styles.viewHelpCentter}>
                    <View style={styles.viewItemTextleft}>
                      <Text style={styles.textLeft}>Thông báo pháp lý</Text>
                    </View>
                    <View style={styles.viewItemTextright}>
                      <View style={styles.Switch}>
                        <Icon name='chevron-right' color={Colors.lightGray} size={30} />
                      </View>
                    </View>
                  </View>

                </View>
                <View style={styles.hr} />
              </View>
            </ScrollView>
          </View>
        </View>
      </View>
    )
  }
}
