import React, { Component } from 'react'
import {
  View,
  Text,
  TextInput,
  Image,
  ScrollView,
  Switch,
  TouchableOpacity,
  Keyboard
} from 'react-native'
import styles from './Styles/AddTweetScreenStyles'
import { Images, Colors } from '../Themes'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput'
export default class AddTweetScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      switch1Value: false,
      styleKeyBoard: false
      
    }
  }
  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = () => {
    this.setState({ styleKeyBoard: true });
  };

  _keyboardDidHide = () => {
    this.setState({ styleKeyBoard: false });
  };
  render () {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.body}>
          <View style={styles.viewHead}>
            <View style={styles.viewCancel}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Icon name='clear' size={20} color={Colors.colorPrimary} />
              </TouchableOpacity>
            </View>
            <View style={styles.viewHeadLogo} />
            <View style={styles.viewMoreIcon}>
              <TouchableOpacity style={styles.buttonTweet}>
                <Text style={styles.textTweet}>Tweet</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.viewForm}>
            <ScrollView>
              <View style={styles.viewTextForm}>
                <View style={styles.viewAvtar}>
                  <Image source={Images.avatarHome} style={styles.itemAvatar} />
                </View>

                <View style={styles.viewTextContent}>
                  <AutoGrowingTextInput
                    ref='userLogin'
                    autoFocus={true}
                    autoCorrect={false}
                    onSubmitEditing={Keyboard.dismiss}
                    placeholder='Chuyện gì đang xảy ra?'
                    underlineColorAndroid='transparent'
                    style={styles.textInput}
                    onChangeText={user => this.setState({ user })}
                    value={this.state.user}
                    returnKeyType='next'
                    autoCapitalize='none'
                  />
                </View>
              </View>
             
              
            </ScrollView>
            <View
                style={
                  this.state.styleKeyBoard
                    ? styles.viewLogin
                    : styles.viewLogin1
                }
              >
                <View style={styles.viewItemLogin}>
                  <View style={styles.viewItemText}>
                    <View style={styles.viewItemText}>
                      <TouchableOpacity>
                        <Icon
                          name='collections'
                          size={24}
                          color={Colors.colorPrimary}
                        />
                      </TouchableOpacity>
                    </View>
                    <View style={styles.viewItemText}>
                      <TouchableOpacity>
                        <Icon
                          name='gif'
                          size={24}
                          color={Colors.colorPrimary}
                        />
                      </TouchableOpacity>
                    </View>
                    <View style={styles.viewItemText}>
                      <TouchableOpacity>
                        <Icon
                          name='toc'
                          size={24}
                          color={Colors.colorPrimary}
                        />
                      </TouchableOpacity>
                    </View>
                    <View style={styles.viewItemText}>
                      <TouchableOpacity>
                        <Icon
                          name='location-on'
                          size={24}
                          color={Colors.colorPrimary}
                        />
                      </TouchableOpacity>
                    </View>
                    <View style={styles.viewItemText}>
                      <TouchableOpacity>
                        <Icon
                          name='slow-motion-video'
                          size={24}
                          color={Colors.colorPrimary}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={styles.viewItemButton}>
                    <TouchableOpacity
                      // onPress={() => this._onLogin()}
                      style={styles.itemButton}
                    >
                      <Icon
                        name='add-circle'
                        size={24}
                        color={Colors.colorPrimary}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                </View>
          </View>
        </View>
      </View>
    )
  }
}
