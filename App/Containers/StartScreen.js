import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import styles from './Styles/StartScreenStyles'
import {Images} from '../Themes'
export default class StartScreen extends Component {
  render () {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.body}>
          <View style={styles.viewLogo}>
            <Image source={Images.logoLogin} style={styles.itemImage} />
          </View>
          <View style={styles.viewButton} >
            <View style={styles.viewTextBig}>
              <Text style={styles.textStart}>Xem chuyện gì đang xảy ra trên thế giới ngay lúc này.</Text>
            </View>

            <View style={styles.itemButton}>
              <TouchableOpacity style={styles.buttonItem} onPress={() => this.props.navigation.navigate('HomeS')} >
                <Text style={styles.textButton}>Bắt đầu</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.viewLogin} >
            <View style={styles.viewLoginText}>
              <View>
                <Text style={styles.textHow}>Bạn đã có một tài khoản? </Text>
              </View>
              <View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('LoginScreen')}>
                  <Text style={styles.textLogin}>Đăng nhập</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }
}
