import React, { Component } from 'react'
import { View, Text } from 'react-native'

export default class componentName extends Component {
  render () {
    return (
      <View style={styles.viewFrame}>
        <View style={styles.viewFrameLeft}>
          <Image
            style={styles.itemAvatar}
            source={Images.avatarHome}
          />
        </View>
        <View style={styles.viewFrameRight}>
          <View style={styles.viewFrameContentLeft}>
            <View style={styles.viewHeadContent}>
              <View style={styles.viewNameContent}>
                <View numberOfLines={1} style={styles.viewName}>
                  <Text style={styles.textNameContent}>YANTV</Text>
                </View>
                <View style={styles.UserNameContent}>
                  <Text
                    numberOfLines={0.1}
                    style={styles.textUserNameContent}
                  >
                    @YANTV
                  </Text>
                </View>
                <View style={styles.UserIconContent}>
                  <Icon
                    name='fiber-manual-record'
                    size={4}
                    color={Colors.darkGray}
                  />
                </View>
                <View style={styles.viewTimeContent}>
                  <Text
                    numberOfLines={1}
                    style={styles.textUserNameContent}
                  >
                    {' '}
                    50 giây
                  </Text>
                </View>
              </View>
              <View style={styles.viewIconSeclect}>
                <TouchableOpacity onPress={this.showActionSheet}>
                  <ActionSheet
                    ref={o => (this.ActionSheet = o)}
                    options={options}
                    cancelButtonIndex={0}
                    destructiveButtonIndex={4}
                    onPress={index => {
                      /* ok */
                    }}
                  />
                  <Icon
                    name='keyboard-arrow-down'
                    size={25}
                    color={Colors.lightGray}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.viewFormContent}>
              <Autolink
                numberOfLines={3}
                style={styles.textItemConten}
                text='THEO BẠN THÍCH VÀ YÊU KHÁC NHAU NHƯ THẾ NÀO? https://github.com/joshswan/react-native-autolink, phone numbers (415-555-5555), emails (josh@example.com), mentions/handles (@twitter), and hashtags (#exciting)'
                hashtag='instagram'
                mention='twitter'
              />
            </View>
          </View>
          <View style={styles.viewFormTweet}>
            <View style={styles.viewItemTweet}>
              <TouchableOpacity>
                <Ionicons
                  size={23}
                  color={Colors.darkGray}
                  name='ios-text-outline'
                />
              </TouchableOpacity>
              <Text style={styles.textTweet}> 2.000</Text>
            </View>
            <View style={styles.viewItemTweet}>
              <TouchableOpacity>
                <Ionicons
                  size={23}
                  color={Colors.darkGray}
                  name='ios-repeat-outline'
                />
              </TouchableOpacity>
              <Text style={styles.textTweet}> 300</Text>
            </View>
            <View style={styles.viewItemTweet}>
              <TouchableOpacity>
                <Ionicons
                  size={23}
                  color={Colors.darkGray}
                  name='ios-heart-outline'
                />
              </TouchableOpacity>
              <Text style={styles.textTweet}> 2.000</Text>
            </View>
            <View style={styles.viewItemTweet}>
              <TouchableOpacity>
                <Ionicons
                  size={23}
                  color={Colors.darkGray}
                  name='ios-log-in-outline'
                />

              </TouchableOpacity>
              <Text style={styles.textTweet}> 2.000</Text>
            </View>
          </View>
        </View>
      </View>
    //   <View style={styles.viewFrame}>
    //     <View style={styles.viewFrameLeft}>
    //       <Image
    //         style={styles.itemAvatar}
    //         source={Images.avatarHome}
    //       />
    //     </View>
    //     <View style={styles.viewFrameRight}>
    //       <View style={styles.viewFrameContentLeft}>
    //         <View style={styles.viewHeadContent}>
    //           <View style={styles.viewNameContent}>
    //             <View style={styles.viewName}>
    //               <Text
    //                 numberOfLines={1}
    //                 style={styles.textNameContent}
    //               >
    //                 MANHVAN
    //               </Text>
    //             </View>
    //             <View style={styles.UserNameContent}>
    //               <Text
    //                 numberOfLines={1}
    //                 style={styles.textUserNameContent}
    //               >
    //                 @MANHV
    //               </Text>
    //             </View>
    //             <View style={styles.UserIconContent}>
    //               <Icon
    //                 name="fiber-manual-record"
    //                 size={4}
    //                 color={Colors.darkGray}
    //               />
    //             </View>
    //             <View style={styles.viewTimeContent}>
    //               <Text
    //                 numberOfLines={1}
    //                 style={styles.textUserNameContent}
    //               >
    //                 {" "}
    //                 50 giây
    //               </Text>
    //             </View>
    //           </View>
    //           <View style={styles.viewIconSeclect}>
    //             <TouchableOpacity>
    //             <ActionSheet
    //                 ref={o => (this.ActionSheet = o)}
    //                 options={options}
    //                 cancelButtonIndex={0}
    //                 destructiveButtonIndex={4}
    //                 onPress={index => {
    //                   /* ok */
    //                 }}
    //               />
    //               <Icon
    //                 name="keyboard-arrow-down"
    //                 size={25}
    //                 color={Colors.lightGray}
    //               />
    //             </TouchableOpacity>
    //           </View>
    //         </View>
    //         <View style={styles.viewFormContent}>
    //           <Autolink
    //             numberOfLines={3}
    //             style={styles.textItemConten}
    //             text="Đẹp trai có gì sai? #xamlon @baybaroi https://github.com/joshswan/react-native-autolink"
    //             hashtag="instagram"
    //             mention="twitter"
    //           />
    //           <Image
    //             source={Images.avatarHome}
    //             style={styles.iTemImageConten}
    //           />
    //         </View>
    //       </View>
    //       <View style={styles.viewFormTweet}>
    //         <View style={styles.viewItemTweet}>
    //           <TouchableOpacity>

    //             <Ionicons
    //               size={23}
    //               color={Colors.darkGray}
    //              name='ios-text-outline'
    //             />
    //           </TouchableOpacity>
    //           <Text style={styles.textTweet}> 2.000</Text>
    //         </View>
    //         <View style={styles.viewItemTweet}>
    //           <TouchableOpacity>
    //           <Ionicons
    //               size={23}
    //               color={Colors.darkGray}
    //              name='ios-repeat-outline'
    //             />
    //           </TouchableOpacity>
    //           <Text style={styles.textTweet}> 300</Text>
    //         </View>
    //         <View style={styles.viewItemTweet}>
    //           <TouchableOpacity>
    //           <Ionicons
    //               size={23}
    //               color={Colors.darkGray}
    //              name='ios-heart-outline'
    //             />
    //           </TouchableOpacity>
    //           <Text style={styles.textTweet}> 2.000</Text>
    //         </View>
    //         <View style={styles.viewItemTweet}>
    //           <TouchableOpacity>
    //           <Ionicons
    //               size={23}
    //               color={Colors.darkGray}
    //              name='ios-log-in-outline'
    //             />
    //           </TouchableOpacity>
    //           <Text style={styles.textTweet}> 2.000</Text>
    //         </View>
    //       </View>
    //     </View>
    // </View>
    )
  }
}
