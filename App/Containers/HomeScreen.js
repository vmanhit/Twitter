/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList
} from "react-native";
import { ActionSheetCustom as ActionSheet } from "react-native-actionsheet";
import Hyperlink from "react-native-hyperlink";
import Autolink from "react-native-autolink";
import NavCustom from "../Navigation/NavCustom";
import { Images, Colors } from "../Themes";
import styles from "./Styles/HomeScreenStyles";
import Icon from "react-native-vector-icons/MaterialIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import datatweet from '../Data/MOCK_DATA.json'
const options = [
  "Huỷ",
  "Thêm vào Tweet vào Khoảng khắc",
  "Tôi không tích Tweet này",
  "Ngừng theo dõi @",
  "Tắt tiếng @",
  "Chặn @",
  "Báo cáo Tweet"
];

export default class HomeScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: (
      <NavCustom
        onPress={() => navigation.navigate("DrawerToggle")}
        iconName="add"
        tittleHead="Trang chủ"
        onPressRight={() => navigation.navigate("MyModal")}
      />
    )
  });

  showActionSheet = () => {
    this.ActionSheet.show();
  };
  _renderNewFeed(){
    var customData = datatweet
    return(
      <FlatList
                  data={customData}
                  renderItem={({ item }) => (
                    <View style={styles.viewFrame}>
                    <View style={styles.viewFrameLeft}>
                      <Image
                        style={styles.itemAvatar}
                        source={{uri:item.avatar}}
                      />
                    </View>
                    <View style={styles.viewFrameRight}>
                      <View style={styles.viewFrameContentLeft}>
                        <View style={styles.viewHeadContent}>
                          <View style={styles.viewNameContent}>
                            <View style={styles.viewName}>
                              <Text
                                numberOfLines={1}
                                style={styles.textNameContent}
                              >
                                {item.fullname}
                              </Text>
                            </View>
                            <View style={styles.UserNameContent}>
                              <Text
                                numberOfLines={1}
                                style={styles.textUserNameContent}
                              >
                               @{item.username}
                              </Text>
                            </View>
                            <View style={styles.UserIconContent}>
                              <Icon
                                name="fiber-manual-record"
                                size={4}
                                color={Colors.darkGray}
                              />
                            </View>
                            <View style={styles.viewTimeContent}>
                              <Text
                                numberOfLines={1}
                                style={styles.textUserNameContent}
                              >
                                {" "}
                                {item.time}
                              </Text>
                            </View>
                          </View>
                          <View style={styles.viewIconSeclect}>
                            <TouchableOpacity>
                            <ActionSheet
                                ref={o => (this.ActionSheet = o)}
                                options={options}
                                cancelButtonIndex={0}
                                destructiveButtonIndex={4}
                                onPress={index => {
                                  /* ok */
                                }}
                              />
                              <Icon
                                name="keyboard-arrow-down"
                                size={25}
                                color={Colors.lightGray}
                              />
                            </TouchableOpacity>
                          </View>
                        </View>
                        <View style={styles.viewFormContent}>
                          <Autolink
                            numberOfLines={3}
                            style={styles.textItemConten}
                            text={item.content}
                            hashtag="instagram"
                            mention="twitter"
                          /> 
                          ({item.image == true ?<Image
                             source={{  uri: item.urlimage }}
                              style={styles.iTemImageConten}
                            /> : null })
                         
                       
                        
                          
                          
                        </View>
                      </View>
                      <View style={styles.viewFormTweet}>
                        <View style={styles.viewItemTweet}>
                          <TouchableOpacity>
            
                            <Ionicons
                              size={23}
                              color={Colors.darkGray}
                             name='ios-text-outline'
                            />
                          </TouchableOpacity>
                          <Text style={styles.textTweet}> {item.nummess}</Text>
                        </View>
                        <View style={styles.viewItemTweet}>
                          <TouchableOpacity>
                          <Ionicons
                              size={23}
                              color={Colors.darkGray}
                             name='ios-repeat-outline'
                            />
                          </TouchableOpacity>
                          <Text style={styles.textTweet}> {item.numtweet}</Text>
                        </View>
                        <View style={styles.viewItemTweet}>
                          <TouchableOpacity>
                          <Ionicons
                              size={23}
                              color={Colors.darkGray}
                             name='ios-heart-outline'
                            />
                          </TouchableOpacity>
                          <Text style={styles.textTweet}> {item.numheart}</Text>
                        </View>
                        <View style={styles.viewItemTweet}>
                          <TouchableOpacity>
                          <Ionicons
                              size={23}
                              color={Colors.darkGray}
                             name='ios-log-in-outline'
                            />
                          </TouchableOpacity>
                          <Text style={styles.textTweet}> {item.numshare}</Text>
                        </View>
                      </View>
                    </View>
                </View>

                  )}
                  keyExtractor={(item, index) => index.toString()}
                />
                )
  }
    render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <View style={styles.viewcContent}>
            <ScrollView>
              <View style={styles.viewItem}>
               {this._renderNewFeed()}
               </View>
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}
