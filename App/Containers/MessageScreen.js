/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView
} from "react-native";
import Hyperlink from "react-native-hyperlink";
import Autolink from "react-native-autolink";
import NavCustom from "../Navigation/NavCustom";
import { Images, Colors } from "../Themes";
import styles from "./Styles/MessageScreenStyles";
import Icon from "react-native-vector-icons/MaterialIcons";
export default class MessageScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: <NavCustom onPress={() => navigation.navigate("DrawerToggle")} iconName ='message' tittleHead = 'Tin nhắn'  />
  });
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <View style={styles.viewcContent}>
            <ScrollView>
              <View style={styles.viewItem}>
                <View style={styles.viewFrame}>
                  <View style={styles.viewFrameLeft}>
                    <Image
                      style={styles.itemAvatar}
                      source={Images.avatarHome}
                    />
                  </View>
                  <View style={styles.viewFrameRight}>
                    <View style={styles.viewFrameContentLeft}>
                      <View style={styles.viewHeadContent}>
                        <View style={styles.viewNameContent}>
                          <View  numberOfLines={1} style={styles.viewName}>
                            <Text style={styles.textNameContent}>Đoàn Viên</Text>
                          </View>
                          <View style={styles.UserNameContent}>
                            <Text  numberOfLines={1} style={styles.textUserNameContent}>
                              @doanxua.vu
                            </Text>
                          </View>
                          <View style={styles.UserIconContent}>
                            <Icon
                              name="fiber-manual-record"
                              size={4}
                              color={Colors.darkGray}
                            />
                          </View>
                          <View style={styles.viewTimeContent}>
                            <Text  numberOfLines={1} style={styles.textUserNameContent}>
                              {" "}
                              50 giây
                            </Text>
                          </View>
                        </View>
                        <View style={styles.viewIconSeclect}>
                          <TouchableOpacity>
                            <Icon
                              name="keyboard-arrow-down"
                              size={25}
                              color={Colors.lightGray}
                            />
                          </TouchableOpacity>
                        </View>
                      </View>
                      <View style={styles.viewFormContent}>
                        <Text  numberOfLines={3} style={styles.textItemConten}>
                         Hello
                      </Text>
                      </View>
                    </View>
                    
                  </View>
                </View>
                
                
              </View>
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}
