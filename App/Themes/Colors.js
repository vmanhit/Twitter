const colors = {
  colorPrimary: '#1DA1F2',
  black: '#14171A',
  darkGray: '#657786',
  lightGray: '#AAB8C2',
  extraLightGray: '#E1E8ED',
  extraExtraLightGray: '#F5F8FA',
  white: '#FFFFFF',
  background: 'rgba(133, 201, 197, 1)',
  backgroundBL: 'rgba(230, 236, 240, 1)',
  hr: 'rgba(210, 210, 210, 1)'

}

export default colors
