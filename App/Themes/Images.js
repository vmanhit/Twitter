// leave off @2x/@3x
const images = {
  logoLogin: require('../Images/Twitter_Logo_Blue.png'),
  avatarHome: require('../Images/AvatarH.jpg'),
  tweetMess: require('../Images/tweetMess.png'),
  tweetHeart: require('../Images/tweetHeart.png'),
  tweetTweet: require('../Images/tweetTweet.png'),
  tweetMail: require('../Images/tweetMail.png')
}

export default images
// leave off @2x/@3x
