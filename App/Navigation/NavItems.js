import React from 'react'
import { Actions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/Ionicons'
// import { Thumbnail } from 'native-base'
import { Colors, Metrics } from '../Themes'

export default {
  openDrawer () {
    Actions.drawerOpen()
  },

  back () {
    Actions.pop()
  },

  openScan () {
    Actions.scanTableScreen()
  },

  backButton () {
    return (
      <Icon name='ios-arrow-back'
        size={Metrics.icons.medium}
        color={Colors.snow}
      />
    )
  },

  hamburgerButton () {
    return (
      <Icon name='md-menu'
        size={Metrics.icons.medium}
        color={Colors.snow}
      />
    )
  }

}
