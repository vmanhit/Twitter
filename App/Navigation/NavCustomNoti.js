import React from 'react'
import styles from './Styles/NavCustomStyles'
import {Images, Metrics, Colors} from '../Themes'
import PropTypes from 'prop-types'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { View, Text, TouchableOpacity, Image, BackHandler, ViewPropTypes } from 'react-native'
class NavCustom extends React.Component {

  static propTypes = {
    move: PropTypes.object,

  }

  static defaultProps = {
    move: {},
    
  }
  constructor (props) {
    super(props)
   
    this.state = {
      showSearchbar: false
    }
  }
  renderLogoImage = () =>{
    const { tittleHead} = this.props
    return (
      <View style={styles.viewNavLogo}><Text style={styles.textHome}>{tittleHead}</Text></View>
    )
  }
  renderBack =() =>{
    return (
      <View />
    )
  }
  renderItemUser =() =>{
    const { onPress } = this.props;
    return (
      <View style={styles.viewItemUser}>
        <TouchableOpacity onPress={onPress}>
          <Image style={styles.itemAvatar} source={Images.avatarHome} />
        </TouchableOpacity>
      </View>
    )
  }
  renderIconRight =()=> {
    const {iconName} = this.props
    return (
      <View style={styles.viewRight}>
      <TouchableOpacity>
        <Icon name={iconName} size={20} color={Colors.colorPrimary} />
      </TouchableOpacity>

      </View>
    )
  }
  render () {
    console.log(this.props)
    return (
      <View style={styles.viewHeader}>
        <View style={styles.header} >
          {this.renderItemUser()}
          {this.renderLogoImage()}
          {this.renderIconRight()}
        </View>
      </View>
    )
  }
}
export default (NavCustom)
