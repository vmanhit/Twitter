import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes'

export default StyleSheet.create({
  header: {
    backgroundColor: Colors.backgroundColor
  },
  icon: {
    height: 20,
    width: 20
  },
  navigationBarStyle: {
    backgroundColor: Colors.snow
  },
  titleStyle: {
    color: '#5c5c5c',
    alignSelf: 'center'
  },
  leftButtonStyle: {
    width: 25,
    height: 20

  },
  rightButtonStyle: {
    width: 50
  },
  leftButtonIconStyle: {

  },
  book: {
    color: 'white'
  },
  aa: {
    width: 80
  },
  navBar: {
    backgroundColor: 'transparent'
  },
  IconRightStyle: {
    width: 20,
    height: 20
  },
  IconRightStyle1: {
    width: 17,
    height: 20
  },
  iconPush: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: 'rgb(252,84,126)',
    position: 'absolute',
    top: 2,
    right: 5,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 2
  },
  textIconPush: {
    textAlign: 'center',
    fontSize: 11,
    fontFamily: 'HelveticaNeue-Light',
    color: 'white'
  },
  viewTab: {
    width: 65,
    height: 40,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: 5
  }
})
