import { StyleSheet, Platform } from 'react-native'
import { Colors, Fonts, Metrics } from '../../Themes'

export default StyleSheet.create({

  header: {
    backgroundColor: Colors.white,
    height: 64,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16,
    alignContent: 'center',
    borderBottomColor: Colors.extraLightGray,
    borderBottomWidth: 1
  },
  viewRight: {

    // backgroundColor: Colors.colorPrimary,
    marginTop: Metrics.marginHorizontal,
    flexDirection: 'row',
    flex: 0.3,
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  viewNavLogo: {

    flexDirection: 'row'

  },
  itemAvatar: {
    marginTop: Metrics.marginHorizontal,
    width: Platform.OS === 'ios' ? 35 : 40,
    height: Platform.OS === 'ios' ? 35 : 40,
    borderRadius: Platform.OS === 'ios' ? 17.5 : 20
  },
  textHome: {
    marginTop: Metrics.marginHorizontal,
    fontWeight: '800',
    fontSize: 16,
    fontFamily: Fonts.type.typeface
  },
  imageLogo: {
    // flex: 1,
    width: Platform.OS === 'ios' ? 130 : 45,
    height: Platform.OS === 'ios' ? 26 : 40
  },
  viewItemUser: {
    alignItems: 'center',
    // backgroundColor: Colors.colorPrimary,
    flexDirection: 'row',

    flex: 0.3
  },
  navigationBarStyle: {
    backgroundColor: Colors.primaryColor
  },
  titleStyle: {
    ...Fonts.style.bold,
    backgroundColor: Colors.transparent,
    color: Colors.snow,
    alignSelf: 'center'
  },
  icon: {
    backgroundColor: Colors.transparent
  },
  leftButtonStyle: {
    minWidth: 50,
    backgroundColor: Colors.transparent
  },
  rightButtonStyle: {
    minWidth: 50,
    backgroundColor: Colors.transparent
  },
  iconMenu: {
    width: Platform.OS === 'ios' ? 25 : 45,
    height: Platform.OS === 'ios' ? 20 : 40,
    backgroundColor: Colors.transparent
  },
  iconBack: {
    width: Platform.OS === 'ios' ? 25 : 45,
    height: Platform.OS === 'ios' ? 20 : 40,
    backgroundColor: Colors.transparent
  },
  tabBarCustomerStyle: {
    backgroundColor: Colors.snow
  },
  tabBarVenueStyle: {
    backgroundColor: Colors.snow
  },
  tabBarStyle: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'transparent',
    borderTopColor: 'transparent'
  },
  textSearch: {
    backgroundColor: Colors.backgroundBL,
    padding: 5,
    height: 30,
    borderRadius: 30,
    width: '100%'
    // borderWidth: 1
    // borderColor: Colors.lightGray
  },
  viewSearch: {
    // backgroundColor: Colors.colorPrimary,
    flex: 1.8,
    marginTop: Metrics.marginHorizontal,
    marginRight: -18,
    alignContent: 'center',
    alignItems: 'center'
  }
})
