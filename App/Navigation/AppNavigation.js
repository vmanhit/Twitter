import React, {Component} from 'react'
import styles from './Styles/NavigationStyles'
import {Text, Image, Platform, View} from 'react-native'
import { Colors, Images, Metrics } from '../Themes'
import { StackNavigator, TabNavigator, TabBarBottom, DrawerNavigator } from 'react-navigation'
import Icon from 'react-native-vector-icons/dist/MaterialIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import NavCustom from './NavCustom'
import HomeScreen from '../Containers/HomeScreen'
import StartScreen from '../Containers/StartScreen'
import LoginScreen from '../Containers/LoginScreen'
import InfoTwitterScreen from '../Containers/InfoTwitterScreen'
import DrawerScreen from '../Components/DrawerScreen'
import MessageScreen from '../Containers/MessageScreen'
import NotificationsScreen from '../Containers/NotificationsScreen'
import AddTweetScreen from '../Containers/AddTweetScreen'
import SearchScreen from '../Containers/SearchScreen'
import Test from '../Containers/Test'

const HomeStack = StackNavigator(

  {
    HomeScreen: {
      screen: HomeScreen
    },
    MyModal: {
      screen: AddTweetScreen,

      navigationOptions: {
        header: 'none',
        tabBarVisible: false
      }
    }
  },
  {

    mode: 'modal',
    initialRouteName: 'HomeScreen',
    navigationOptions: {
      // header: 'none',
      // tabBarVisible: false
    }
  }
)
const SearchStack = StackNavigator(

  {
    SearchScreen: {
      screen: SearchScreen
    }
  },
  {
    initialRouteName: 'SearchScreen',
    navigationOptions: {
      // header: 'none',
      // tabBarVisible: false
    }
  }
)
const NotificationsStack = StackNavigator(

  {
    NotificationsScreen: {
      screen: NotificationsScreen
    }
  },
  {
    initialRouteName: 'NotificationsScreen',
    navigationOptions: {
      // header: 'none',
      // tabBarVisible: false
    }
  }
)
const MessageStack = StackNavigator(

  {
    MessageScreen: {
      screen: MessageScreen
    }
  },
  {
    initialRouteName: 'MessageScreen',
    navigationOptions: {
      // header: 'none',
      // tabBarVisible: false
    }
  }
)

const loginStack = StackNavigator(

  {
    StartScreen: {
      screen: StartScreen
    },
    LoginScreen: {
      screen: LoginScreen
    }
  },
  {
    initialRouteName: 'StartScreen',
    navigationOptions: {
      header: 'none'
      // tabBarVisible: false
    }
  }
)
const DrawerScreenq = StackNavigator(

  {
    Test: {
      screen: Test
    },
    LoginScreen: {
      screen: LoginScreen
    }
  },
  {
    initialRouteName: 'Test',
    navigationOptions: {
      header: 'none'
      // tabBarVisible: false
    }
  }
)

const Tabar = TabNavigator(
  {
    Home: {screen: HomeStack},
    Search: { screen: SearchStack },
    Notifications: { screen: NotificationsStack },
    Message: { screen: MessageStack }
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state
        let iconName
        if (routeName === 'Home') {
          iconName = 'home'
        } else if (routeName === 'Search') {
          iconName = 'search'
        } else if (routeName === 'Notifications') {
          iconName = 'notifications-none'
        } else if (routeName === 'Message') {
          iconName = 'mail-outline'
        }
        return <Icon name={iconName} size={27} color={tintColor} />
      }
    }),
    tabBarOptions: {
      activeTintColor: Colors.colorPrimary,
      inactiveTintColor: Colors.lightGray,
      showLabel: false,
      style: {
        backgroundColor: '#fff'
      }

    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false
  }
)

const StackDrawer = DrawerNavigator({
  LoginS: {
    screen: loginStack
  },
  HomeS: {
    screen: Tabar
  },
  MyDrwaer: {
    screen: DrawerScreenq
  }
},
  {
    initialRouteName: 'LoginS',
    drawerWidth: Metrics.screenWidth / 1.17,
    drawerPosition: 'left',
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
    contentComponent: DrawerScreen,
    contentOptions: {
      activeTintColor: '#00C99D',
      iconContainerStyle: {
        width: 50,
        height: 50,
        justifyContent: 'center',
        paddingLeft: 5,
        marginLeft: 0,
        marginRight: 0
      },
      itemsContainerStyle: {
        borderBottomWidth: 1,
        borderBottomColor: 'grey',
        marginLeft: 0
      }
    }
  }
)
export default StackDrawer
